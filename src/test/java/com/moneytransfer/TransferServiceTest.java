package com.moneytransfer;

import com.moneytransfer.account.Account;
import com.moneytransfer.transfer.Dto;
import com.moneytransfer.utils.DbUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    com.moneytransfer.account.Dao accountDao;

    @Mock
    com.moneytransfer.transfer.Dao transferDao;

    @InjectMocks
    com.moneytransfer.transfer.ServiceImpl transferService;

    private static App app;

    private static final String TRANSFERS_URL = "http://localhost:9080/money-transfer/transfers";

    @BeforeClass
    public static void initApp() throws Exception {
        app = TestUtils.initApp();
        app.startServer();
    }

    @Test
    public void transferAmountTest() {
        long IvanId = 1L;
        long MaxId = 2L;

        Account from = new Account("Ivan", BigDecimal.valueOf(1000), "RUR");
        from.setAccountId(IvanId);

        Account to = new Account("Max", BigDecimal.valueOf(700), "RUR");
        to.setAccountId(MaxId);

        when(accountDao.getAccount(eq(IvanId), any(Connection.class))).thenReturn(from);
        when(accountDao.getAccount(eq(MaxId), any(Connection.class))).thenReturn(to);

        Dto dto = new Dto();
        dto.setFrom(IvanId);
        dto.setTo(MaxId);
        dto.setAmount(BigDecimal.TEN);

        transferService.transferAmount(dto);
        verify(accountDao, times(2)).getAccount(anyLong(), any(Connection.class));
    }

    @Test
    public void transferAmountIntegrationTest() throws Exception {
        try (Connection connection = DbUtils.getConnection();
             Statement stmt = connection.createStatement()) {

            connection.setAutoCommit(false);

            stmt.execute("INSERT INTO ACCOUNT(ID, RECORD_VERSION, NAME, AMOUNT, CURRENCY_CODE) " +
                    "VALUES(10, 1, 'Max', 3500, 'USD')");

            stmt.execute("INSERT INTO ACCOUNT(ID, RECORD_VERSION, NAME, AMOUNT, CURRENCY_CODE) " +
                    "VALUES(12, 1, 'Vlad', 2100, 'USD')");

            connection.commit();
        }

        HttpURLConnection httpURLConnection = prepareConnection();
        assertEquals(HttpStatus.NO_CONTENT_204, httpURLConnection.getResponseCode());

        try (Connection connection = DbUtils.getConnection();
             Statement stmt = connection.createStatement()) {

            ResultSet rs = stmt.executeQuery("SELECT * FROM ACCOUNT WHERE ID IN (10, 12)");

            Map<Long, Account> accountMap = new HashMap<>();
            while (rs.next()) {
                Account account = new Account(
                        rs.getString("NAME"),
                        rs.getBigDecimal("AMOUNT"),
                        rs.getString("CURRENCY_CODE")
                );

                accountMap.put(rs.getLong("ID"), account);
                System.out.println(account);
            }

            assertEquals("Max", accountMap.get(10L).getUserName());
            assertEquals(BigDecimal.valueOf(3100), accountMap.get(10L).getAmount());
            assertEquals("USD", accountMap.get(10L).getCurrencyCode());

            assertEquals("Vlad", accountMap.get(12L).getUserName());
            assertEquals(BigDecimal.valueOf(2500), accountMap.get(12L).getAmount());
            assertEquals("USD", accountMap.get(12L).getCurrencyCode());
        }
    }

    private HttpURLConnection prepareConnection() throws IOException {
        String dataString = "{\"from\":\"10\", \"to\":\"12\", \"amount\":\"400\"}";
        HttpURLConnection httpCreate =
                (HttpURLConnection)new URL(TRANSFERS_URL).openConnection();
        httpCreate.setRequestMethod("POST");
        httpCreate.setRequestProperty("Content-Type", "application/json");
        httpCreate.setRequestProperty("Accept", "application/json");
        httpCreate.setDoOutput(true);
        try(DataOutputStream dataOutputStream =
                    new DataOutputStream(httpCreate.getOutputStream())) {
            byte[] postData = dataString.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(postData, 0, postData.length);
        }

        return httpCreate;
    }

    @AfterClass
    public static void stopServer() throws Exception {
        app.stopServer();
    }
}
