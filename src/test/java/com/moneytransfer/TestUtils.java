package com.moneytransfer;

import com.moneytransfer.infrastructure.h2.H2Initializer;
import com.moneytransfer.utils.DbUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class TestUtils {

    private static volatile App app;

    static App initApp() {
        App localApp = app;
        if (localApp == null) {
            synchronized (TestUtils.class) {
                localApp = app;
                if (localApp == null) {
                    app = new App();
                    app.createServer();
                    app.bindGuiceContextToServer();

                    DbUtils.loadH2Config();
                    H2Initializer.initDb();
                }
            }
        }

        return app;
    }
}
