package com.moneytransfer;

import com.moneytransfer.account.Account;
import com.moneytransfer.utils.DbUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    com.moneytransfer.account.Dao accountDao;

    @InjectMocks
    com.moneytransfer.account.ServiceImpl accountService;

    private static App app;

    private static final String ACCOUNTS_URL = "http://localhost:9080/money-transfer/accounts";

    @BeforeClass
    public static void initApp() throws Exception {
        app = TestUtils.initApp();
        app.startServer();
    }

    @Test
    public void getAllAccountsTest() throws Exception {
        HttpURLConnection http =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        http.connect();
        assertEquals(http.getResponseCode(), HttpStatus.OK_200);
    }

    @Test
    public void getAllAccountsMockTest() {
        List<Account> accounts = new ArrayList<>();
        when(accountDao.getAccounts(any(Connection.class))).thenReturn(accounts);

        assertEquals(accounts, accountService.getAccounts());
        verify(accountDao, times(1)).getAccounts(any(Connection.class));
    }

    @Test
    public void getAccountTest() {
        Account account = new Account();
        when(accountDao.getAccount(anyLong(), any(Connection.class))).thenReturn(account);

        assertEquals(account, accountService.getAccount(1L));
        verify(accountDao, times(1)).getAccount(anyLong(), any(Connection.class));
    }

    @Test
    public void updateAccountTest() {
        accountService.updateAccount(new Account());
        verify(accountDao, times(1)).updateAccount(any(Account.class), any(Connection.class));
    }

    @Test
    public void createAccountTest() {
        accountService.createAccount(new Account());
        verify(accountDao, times(1)).createAccount(any(Account.class), any(Connection.class));
    }

    @Test
    public void createAndUpdateAccountIntegrationTest() throws Exception {
        HttpURLConnection httpCreate = prepareCreateConnection();
        assertEquals(HttpStatus.NO_CONTENT_204, httpCreate.getResponseCode());

        Account account = new Account();
        String sql = "SELECT * FROM ACCOUNT WHERE NAME = 'Ivan'";
        try (Connection connection = DbUtils.getConnection();
             Statement stmt = connection.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                account.setAccountId(rs.getLong("ID"));
                account.setUserName(rs.getString("NAME"));
                account.setCurrencyCode(rs.getString("CURRENCY_CODE"));
                account.setAmount(rs.getBigDecimal("AMOUNT"));
                account.setRecordedVersion(rs.getLong("RECORD_VERSION"));
            }
            System.out.println(account);

            assertEquals("Ivan", account.getUserName());
            assertEquals(BigDecimal.valueOf(1000), account.getAmount());
            assertEquals("RUR", account.getCurrencyCode());
        }

        HttpURLConnection httpUpdate = prepareUpdateConnection(account);
        assertEquals(HttpStatus.NO_CONTENT_204, httpUpdate.getResponseCode());

        try (Connection connection = DbUtils.getConnection();
             Statement stmt = connection.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                account.setCurrencyCode(rs.getString("CURRENCY_CODE"));
                account.setAmount(rs.getBigDecimal("AMOUNT"));
            }
            System.out.println(account);

            assertEquals(BigDecimal.valueOf(11500), account.getAmount());
            assertEquals("USD", account.getCurrencyCode());
        }
    }

    @AfterClass
    public static void stopServer() throws Exception {
        app.stopServer();
    }

    private HttpURLConnection prepareCreateConnection() throws IOException {
        String accountString = "{\"userName\":\"Ivan\", \"amount\":\"1000\", \"currencyCode\":\"RUR\"}";
        HttpURLConnection httpCreate =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        httpCreate.setRequestMethod("POST");
        httpCreate.setRequestProperty("Content-Type", "application/json");
        httpCreate.setRequestProperty("Accept", "application/json");
        httpCreate.setDoOutput(true);
        try(DataOutputStream dataOutputStream =
                    new DataOutputStream(httpCreate.getOutputStream())) {
            byte[] postData = accountString.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(postData, 0, postData.length);
        }

        return httpCreate;
    }

    private HttpURLConnection prepareUpdateConnection(Account account) throws IOException {
        String accountId = String.valueOf(account.getAccountId());
        String updatedAccountString = "{\"accountId\":\"" + accountId + "\", \"recordedVersion\":\"1\", " +
                "\"userName\":\"Ivan\", \"amount\":\"11500\", \"currencyCode\":\"USD\"}";
        HttpURLConnection httpUpdate =
                (HttpURLConnection)new URL(ACCOUNTS_URL).openConnection();
        httpUpdate.setRequestMethod("PUT");
        httpUpdate.setRequestProperty("Content-Type", "application/json");
        httpUpdate.setRequestProperty("Accept", "application/json");
        httpUpdate.setDoOutput(true);
        try(DataOutputStream dataOutputStream =
                    new DataOutputStream(httpUpdate.getOutputStream())) {
            byte[] postData = updatedAccountString.getBytes(StandardCharsets.UTF_8);
            dataOutputStream.write(postData, 0, postData.length);
        }

        return httpUpdate;
    }
}
