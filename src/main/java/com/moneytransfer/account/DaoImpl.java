package com.moneytransfer.account;

import com.moneytransfer.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoImpl implements Dao {

    private Logger logger = Logger.getLogger(DaoImpl.class.toString());

    @Override
    public int createAccount(Account account, Connection connection) {
        String sql = "INSERT INTO ACCOUNT(ID, RECORD_VERSION, NAME, AMOUNT, CURRENCY_CODE) " +
                "VALUES (NEXTVAL('ID_SEQ'), NEXTVAL('REC_SEQ'), ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, account.getUserName());
            ps.setBigDecimal(2, account.getAmount());
            ps.setString(3, account.getCurrencyCode());
            return ps.executeUpdate();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during initializing account", e);
            throw new RuntimeException("Exception during initializing account");
        }
    }

    @Override
    public int updateAccount(Account account, Connection connection) {
        String updateQuery = "UPDATE ACCOUNT SET AMOUNT = ?, CURRENCY_CODE = ?, " +
                "RECORD_VERSION = NEXTVAL('REC_SEQ') WHERE ID = ? AND RECORD_VERSION = ?";
        String selectQuery = "SELECT RECORD_VERSION FROM ACCOUNT WHERE ID = ?";
        try (PreparedStatement updateStatement = connection.prepareStatement(updateQuery);
            PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {

            selectStatement.setLong(1, account.getAccountId());
            long currentVersion = DbUtils.getCurrentVersion(selectStatement.executeQuery());

            if (account.getRecordedVersion() != currentVersion) {
                throw new RuntimeException("Transfer failed, try again later");
            }
            updateStatement.setBigDecimal(1, account.getAmount());
            updateStatement.setString(2, account.getCurrencyCode());
            updateStatement.setLong(3, account.getAccountId());
            updateStatement.setLong(4, account.getRecordedVersion());
            return updateStatement.executeUpdate();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during updating account", e);
            throw new RuntimeException("Exception during updating account");
        }
    }

    @Override
    public void deleteAccount(long accountId, Connection connection) {
        String sql = "DELETE FROM ACCOUNT WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, accountId);
            ps.execute();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during deleting account", e);
            throw new RuntimeException("Exception during deleting account");
        }
    }

    @Override
    public Account getAccount(long accountId, Connection connection) {
        String sql = "SELECT * FROM ACCOUNT WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, accountId);
            ResultSet rs = ps.executeQuery();

            Account account = new Account();
            if (rs.next()) {
                account.setAccountId(rs.getLong("ID"));
                account.setUserName(rs.getString("NAME"));
                account.setAmount(rs.getBigDecimal("AMOUNT"));
                account.setCurrencyCode(rs.getString("CURRENCY_CODE"));
                account.setRecordedVersion(rs.getLong("RECORD_VERSION"));
            }

            return account;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during getting account", e);
            throw new RuntimeException("Exception during getting account");
        }
    }

    @Override
    public List<Account> getAccounts(Connection connection) {
        String sql = "SELECT * FROM ACCOUNT";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();
            List<Account> accounts = new ArrayList<>();
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getLong("ID"));
                account.setUserName(rs.getString("NAME"));
                account.setAmount(rs.getBigDecimal("AMOUNT"));
                account.setCurrencyCode(rs.getString("CURRENCY_CODE"));
                account.setRecordedVersion(rs.getLong("RECORD_VERSION"));
                accounts.add(account);
            }

            return accounts;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during getting accounts", e);
            throw new RuntimeException("Exception during getting accounts");
        }
    }
}
