package com.moneytransfer.account;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Account {

    /**
     * Unique account id
     */
    private long accountId;

    /**
     * Recorded version
     * in db for optimistic locking check
     */
    private long recordedVersion;

    /**
     * User name
     */
    private String userName;

    /**
     * Current user's amount
     */
    private BigDecimal amount;

    /**
     * Currency code of current amount
     */
    private String currencyCode;

    public Account(String userName, BigDecimal amount, String currencyCode) {
        this.userName = userName;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }
}
