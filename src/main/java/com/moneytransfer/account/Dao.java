package com.moneytransfer.account;

import java.sql.Connection;
import java.util.List;

public interface Dao {
    /**
     * Create new account
     *
     * @param account - account
     * @param connection - DB connection
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     * or (2) 0 for SQL statements that return nothing
     */
    int createAccount(Account account, Connection connection);

    /**
     * Update current account
     *
     * @param account - account
     * @param connection - DB connection
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements
     * or (2) 0 for SQL statements that return nothing
     */
    int updateAccount(Account account, Connection connection);

    /**
     * Delete account
     *
     * @param accountId - account id
     * @param connection - DB connection
     */
    void deleteAccount(long accountId, Connection connection);

    /**
     * Fetch account
     *
     * @param accountId - account id
     * @param connection - DB connection
     * @return account
     */
    Account getAccount(long accountId, Connection connection);

    /**
     * Fetch all accounts
     *
     * @param connection - DB connection
     * @return List of accounts
     */
    List<Account> getAccounts(Connection connection);
}
