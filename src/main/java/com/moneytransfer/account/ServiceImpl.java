package com.moneytransfer.account;

import com.google.inject.Inject;
import com.moneytransfer.utils.TransactionUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceImpl implements Service {

    private Logger logger = Logger.getLogger(ServiceImpl.class.toString());

    private Dao accountDao;

    @Inject
    public ServiceImpl(Dao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void createAccount(Account account) {
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(true);
                accountDao.createAccount(account, connection);
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during initializing account", e);
                throw new RuntimeException("Exception during initializing account");
            }
        }, Connection.TRANSACTION_READ_COMMITTED);
    }

    @Override
    public void updateAccount(Account account) {
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(true);
                accountDao.updateAccount(account, connection);
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during updating account", e);
                throw new RuntimeException("Exception during updating account");
            }
        }, Connection.TRANSACTION_SERIALIZABLE);
    }

    @Override
    public void deleteAccount(long accountId) {
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(true);
                accountDao.deleteAccount(accountId, connection);
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during deleting account", e);
                throw new RuntimeException("Exception during deleting account");
            }
        }, Connection.TRANSACTION_READ_COMMITTED);
    }

    @Override
    public Account getAccount(long accountId) {
        AtomicReference<Account> accountReference = new AtomicReference<>();
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(true);
                accountReference.set(accountDao.getAccount(accountId, connection));
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during getting account", e);
                throw new RuntimeException("Exception during getting account");
            }
        }, Connection.TRANSACTION_READ_COMMITTED);

        return accountReference.get();
    }

    @Override
    public List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(true);
                accounts.addAll(accountDao.getAccounts(connection));
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during getting accounts", e);
                throw new RuntimeException("Exception during getting accounts");
            }
        }, Connection.TRANSACTION_READ_COMMITTED);

        return accounts;
    }
}
