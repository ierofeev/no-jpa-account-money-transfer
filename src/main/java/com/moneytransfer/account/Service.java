package com.moneytransfer.account;

import java.util.List;

public interface Service {

    /**
     * Create new account
     *
     * @param account
     * @return
     */
    void createAccount(Account account);

    /**
     * Update current account
     *
     * @param account
     * @return
     */
    void updateAccount(Account account);

    /**
     * Delete account
     *
     * @param accountId
     */
    void deleteAccount(long accountId);

    /**
     * Fetch account
     *
     * @param accountId
     * @return
     */
    Account getAccount(long accountId);

    /**
     * Fetch accounts
     *
     * @return
     */
    List<Account> getAccounts();
}
