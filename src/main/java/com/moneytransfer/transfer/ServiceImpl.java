package com.moneytransfer.transfer;

import com.google.inject.Inject;
import com.moneytransfer.account.Account;
import com.moneytransfer.utils.TransactionUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceImpl implements Service {

    private static Logger logger = Logger.getLogger(ServiceImpl.class.toString());

    private com.moneytransfer.account.Dao accountDao;
    private com.moneytransfer.transfer.Dao transferDao;

    @Inject
    public ServiceImpl(com.moneytransfer.account.Dao accountDao,
                       com.moneytransfer.transfer.Dao transferDao) {
        this.accountDao = accountDao;
        this.transferDao = transferDao;
    }

    @Override
    public void transferAmount(Dto dto) {
        TransactionUtils.transact((Connection connection) -> {
            try {
                connection.setAutoCommit(false);
                Account from = accountDao.getAccount(dto.getFrom(), connection);
                Account to = accountDao.getAccount(dto.getTo(), connection);

                if (isTransferValid(from, to, dto.getAmount())) {
                    transferDao.transferAmount(from, to, dto.getAmount(), connection);
                }
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Exception during transfer amount", e);
                throw new RuntimeException("Exception during transfer amount");
            }
        }, Connection.TRANSACTION_READ_COMMITTED);
    }

    private boolean isTransferValid(Account fromAccount, Account toAccount, BigDecimal amount) {
        if (fromAccount != null && toAccount != null) {
            BigDecimal fromAmount = fromAccount.getAmount();
            if (fromAmount.subtract(amount).compareTo(BigDecimal.ZERO) > -1) {
                return true;
            } else {
                logger.log(Level.WARNING, "Exception during transfer amount, amount is too big: from " +
                        fromAccount.getAccountId() + " to " + toAccount.getAccountId() + " : amount " + amount.doubleValue());
                throw new RuntimeException("Exception during transfer amount, amount is too big");
            }
        } else {
            logger.log(Level.WARNING, "Exception during transfer amount, account can't be null");
            throw new RuntimeException("Exception during transfer amount, account can't be empty");
        }
    }
}
