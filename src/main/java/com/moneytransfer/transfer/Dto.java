package com.moneytransfer.transfer;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Dto {
    /**
     * Write-off account
     */
    private long from;

    /**
     * Enrollment account
     */
    private long to;


    private BigDecimal amount;
}
