package com.moneytransfer.transfer;

import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/money-transfer/transfers")
@RequestScoped
public class Controller {

    private Service service;

    @Inject
    public Controller(Service service) {
        this.service = service;
    }

    /**
     * Handler for POST request to transfer amount
     * from one account to another
     *
     * @param dto - data transfer object
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void transferAmount(Dto dto) {
        service.transferAmount(dto);
    }
}
