package com.moneytransfer.transfer;

public interface Service {

    /**
     * Transfer amount from one account to another
     *
     * @param dto - data transfer object
     */
    void transferAmount(Dto dto);
}
