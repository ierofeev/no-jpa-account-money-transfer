package com.moneytransfer.transfer;

import com.moneytransfer.account.Account;

import java.math.BigDecimal;
import java.sql.Connection;

public interface Dao {

    /**
     * Transfer amount from one to another account
     *
     * @param from - from account
     * @param to - to account
     * @param amount -amount to transfer
     * @param connection - db connection
     */
    void transferAmount(Account from, Account to, BigDecimal amount, Connection connection);
}
