package com.moneytransfer.transfer;

import com.moneytransfer.account.Account;
import com.moneytransfer.utils.DbUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoImpl implements Dao {

    private static Logger logger = Logger.getLogger(DaoImpl.class.toString());

    @Override
    public void transferAmount(Account from, Account to, BigDecimal amount, Connection connection) {
        String updateQuery = "UPDATE ACCOUNT SET AMOUNT = ?, RECORD_VERSION = NEXTVAL('REC_SEQ') " +
                "WHERE ID = ? AND RECORD_VERSION = ?";
        String selectQuery = "SELECT RECORD_VERSION FROM ACCOUNT WHERE ID = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
             PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {

            selectStatement.setLong(1, from.getAccountId());
            long fromCurrentVersion = DbUtils.getCurrentVersion(selectStatement.executeQuery());

            selectStatement.setLong(1, to.getAccountId());
            long toCurrentVersion  = DbUtils.getCurrentVersion(selectStatement.executeQuery());

            if (from.getRecordedVersion() != fromCurrentVersion || to.getRecordedVersion() != toCurrentVersion) {
                throw new RuntimeException("Transfer failed, try again later");
            }

            preparedStatement.setBigDecimal(1, from.getAmount().subtract(amount));
            preparedStatement.setLong(2, from.getAccountId());
            preparedStatement.setLong(3, from.getRecordedVersion());
            preparedStatement.addBatch();

            preparedStatement.setBigDecimal(1, to.getAmount().add(amount));
            preparedStatement.setLong(2, to.getAccountId());
            preparedStatement.setLong(3, to.getRecordedVersion());
            preparedStatement.addBatch();

            preparedStatement.executeBatch();
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception during amount transfer from account " + from.getAccountId() +
                    " to account " + to.getAccountId(), e);
            throw new RuntimeException("Exception  during amount transfer");
        }
    }
}
