package com.moneytransfer.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionUtils {

    private static Logger logger = Logger.getLogger(TransactionUtils.class.toString());

    public static void transact(Consumer<Connection> connectionConsumer, int isolationLevel) {
        Connection connection = null;
        try {
            connection = DbUtils.getConnection(isolationLevel);
            connectionConsumer.accept(connection);
            connection.commit();
        } catch (Exception e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, "Exception during rollback connection", e);
                    throw new RuntimeException("Exception during rollback connection");
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Exception during close connection", e);
                    throw new RuntimeException("Exception during close connection");
                }
            }
        }
    }
}
